package com.application.carnetdevol.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.application.carnetdevol.data.VOL_TABLE_NAME
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.model.Voile
import java.time.LocalDateTime

@Entity(tableName = VOL_TABLE_NAME)
class VolEntity(@PrimaryKey(autoGenerate = true) val id: Int,
                var date: Long,
                var heure: Int,
                var site: String,
                var duree: Int,
                //var voile: Int
) {
}