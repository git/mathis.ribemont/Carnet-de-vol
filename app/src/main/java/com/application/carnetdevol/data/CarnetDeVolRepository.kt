package com.application.carnetdevol.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.application.carnetdevol.api.BaliseMeteoManager
import com.application.carnetdevol.api.FfvlManager
import com.application.carnetdevol.data.entity.VolEntity
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.model.Vol
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime

class CarnetDeVolRepository(private val volDao: VolDao,
                            private val baliseMeteoManager: BaliseMeteoManager,
                            private val ffvlManager: FfvlManager) {

    init {
        if (allSite.value == null || allSite.value?.isEmpty() == true){
            ffvlManager.getAllSites().enqueue(object: Callback<List<Site>>{
                override fun onResponse(
                    call: Call<List<Site>>,
                    response: Response<List<Site>>
                ) {
                    Log.d("CarnetDeVolRepository: getSites","Données récupérées: ${response.body()?.size.toString()} éléments.")
                    allSite.postValue(response.body())
                }

                override fun onFailure(call: Call<List<Site>>, t: Throwable) {
                    Log.e("CarnetDeVolRepository: getAllSites", "Erreur durant la récupération des données.\n"
                            + t.stackTraceToString())
                }

            })
        }
    }

    fun getAllVols(): LiveData<List<Vol>>{
        return Transformations.map(volDao.getAllVols()) { entities ->
            val list = mutableListOf<Vol>()

            for (item: VolEntity in entities) {
                list.add(
                    entityToVol(item)
                )
            }
            list
        }
    }

    fun insert(vol: Vol) {
        CoroutineScope(Dispatchers.IO).launch { volDao.insert(volToEntity(vol)) }
    }

    fun getAllSites(): LiveData<List<Site>> {
        Log.d("CarnetDeVolRepository: getAllVols","Récupération des sites.")
//        val allSite = volDao.getAllSites()
//        allSite.observeForever {
//            Log.d("CarnetDeVolRepository: getAllVols","allSite: $allSite, \n" +
//                    "allSite.value: ${allSite.value}\n" +
//                    "allSite.value.size: ${allSite.value?.size}")
//            if(it.isEmpty()){
//                ffvlManager.getAllSites().enqueue(object: Callback<List<Site>>{
//                    override fun onResponse(
//                        call: Call<List<Site>>,
//                        response: Response<List<Site>>
//                    ) {
//                        synchronized(ffvlManager){
//                            Log.d("CarnetDeVolRepository: getSite","Données récupérées: ${response.body()?.size.toString()} éléments.")
//                            if(response.body() != null){
//                                CoroutineScope(Dispatchers.IO).launch { insertAllSite(response.body()!!) }
//                            }
//                        }
//                    }
//
//                    override fun onFailure(call: Call<List<Site>>, t: Throwable) {
//                        Log.e("CarnetDeVolRepository: getAllSites", "Erreur durant la récupération des données.\n"
//                                + t.stackTraceToString())
//                    }
//
//                })
//            }
//        }

        return allSite
    }

    private fun insertAllSite(allSiteList: List<Site>) {
        CoroutineScope(Dispatchers.IO).launch {
            for (site in allSiteList) {
                Log.d("CarnetDeVolRepository: insertAllSite", "Insertion de $site")
                volDao.insertSite(site)
                //ça plante dans le dao, mais pas d'erreur
            }
        }
    }

    fun getVol(volId: Int): LiveData<Vol> {
        return Transformations.map(volDao.getVol(volId)) {
            entityToVol(it)
        }
    }

    fun volToEntity(vol: Vol): VolEntity{
        return VolEntity(vol.id, vol.date.toEpochDay(), vol.heure.toSecondOfDay(), vol.site?.numero ?: "", vol.duree)
    }

    fun entityToVol(entity: VolEntity): Vol {
        return Vol(entity.id, LocalDate.ofEpochDay(entity.date), LocalTime.ofSecondOfDay(entity.heure.toLong()), getSite(entity.site), entity.duree)
    }

    suspend fun update(vol: Vol) = withContext(Dispatchers.IO) {
        val entity = volToEntity(vol)
        volDao.update(entity)
        Log.d("CarnetDeVolRepository: update","MAJ du vol ${entity.id}. Site: ${entity.site}")
    }

    suspend fun delete(vol: Vol) = withContext(Dispatchers.IO) {
        //volDao.delete(volToEntity(vol))
    }

    companion object {
        private val allSite = MutableLiveData<List<Site>>()
    }

    fun getSite(numero: String): Site?{
        Log.d("VolViewModel getSite","${getAllSites().value}")
        if(numero.trim() == ""){
            return null
        }
        return getAllSites().value?.find { it.numero == numero }
    }
}