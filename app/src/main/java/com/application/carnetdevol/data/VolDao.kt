package com.application.carnetdevol.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.application.carnetdevol.data.entity.VolEntity
import com.application.carnetdevol.model.Site

const val SITE_TABLE_NAME = "site_table"
const val VOL_TABLE_NAME = "vol_table"

@Dao
interface VolDao {
    @Query("SELECT * FROM $VOL_TABLE_NAME")
    fun getAllVols(): LiveData<List<VolEntity>>

    @Query("SELECT * FROM $SITE_TABLE_NAME")
    fun getAllSites(): LiveData<List<Site>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vol: VolEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSite(site: Site)

    @Query("SELECT * FROM $VOL_TABLE_NAME WHERE id = :volId")
    fun getVol(volId: Int): LiveData<VolEntity>

    @Update
    suspend fun update(volEntity: VolEntity)

    @Delete
    suspend fun delete(volToEntity: VolEntity)
}