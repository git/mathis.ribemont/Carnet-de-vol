package com.application.carnetdevol.data

import android.app.Application
import android.util.Log
import androidx.lifecycle.asLiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.application.carnetdevol.CarnetDeVolApplication
import com.application.carnetdevol.data.entity.VolEntity
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.model.Vol
import java.util.concurrent.Executors
import kotlin.coroutines.coroutineContext

@Database(entities = [VolEntity::class, Site::class], version = 3, exportSchema = false)
abstract class CarnetDeVolDatabase: RoomDatabase() {
    abstract fun volDao(): VolDao

    companion object {
        private const val DB_NAME = "carnet_de_vol_database"

        private lateinit var application: Application

        @Volatile
        private var instance: CarnetDeVolDatabase? = null

        fun getDatabase(): CarnetDeVolDatabase {
            if (::application.isInitialized) {
                if (instance == null)
                    synchronized(this) {
                        if (instance == null) {
                            var dbBuilder = Room.databaseBuilder(
                                application.applicationContext,
                                CarnetDeVolDatabase::class.java,
                                DB_NAME
                            ).fallbackToDestructiveMigration()
                                /*.setQueryCallback(object: QueryCallback{
                                    override fun onQuery(sqlQuery: String, bindArgs: List<Any?>) {
                                        Log.d("SQL_DB","SQL Query: $sqlQuery SQL Args: $bindArgs")
                                    }

                                }, Executors.newSingleThreadExecutor())*/
                            instance = dbBuilder.build()
                        }
                    }
                return instance!!
            } else
                throw RuntimeException("the database must be first initialized")
        }

        @Synchronized
        fun initialize(app: CarnetDeVolApplication) {
            if (::application.isInitialized)
                throw RuntimeException("the database must not be initialized twice")

            application = app
        }
    }
}