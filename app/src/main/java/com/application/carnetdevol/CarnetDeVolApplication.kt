package com.application.carnetdevol

import android.app.Application
import com.application.carnetdevol.api.BaliseMeteoManager
import com.application.carnetdevol.api.FfvlManager
import com.application.carnetdevol.data.CarnetDeVolDatabase
import com.application.carnetdevol.data.CarnetDeVolRepository

class CarnetDeVolApplication : Application() {
    val database by lazy {
        CarnetDeVolDatabase.initialize(this)
        CarnetDeVolDatabase.getDatabase()
    }
    private val baliseMeteoManager by lazy { BaliseMeteoManager() }
    private val ffvlManager by lazy { FfvlManager() }
    val repository by lazy { CarnetDeVolRepository(database.volDao(), baliseMeteoManager, ffvlManager) }
}