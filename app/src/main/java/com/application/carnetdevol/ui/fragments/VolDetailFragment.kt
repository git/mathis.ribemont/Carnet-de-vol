package com.application.carnetdevol.ui.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import com.application.carnetdevol.R
import com.application.carnetdevol.model.NEW_VOL_ID
import com.application.carnetdevol.model.Utils
import com.application.carnetdevol.ui.activity.SelectSiteActivity
import com.application.carnetdevol.ui.utils.Formatters
import com.application.carnetdevol.viewModel.VolViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.text.DateFormat
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class VolDetailFragment : Fragment() {
    private lateinit var listener: OnDeleteVol
    private lateinit var volVM: VolViewModel
    private var volId = NEW_VOL_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        volId = savedInstanceState?.getInt(EXTRA_VOL_ID) ?: arguments?.getInt(EXTRA_VOL_ID)
                ?: NEW_VOL_ID

        val parentActivity = requireActivity()

        if (volId == NEW_VOL_ID){
            parentActivity.setTitle(R.string.ajouter_vol)
        }

        volVM = VolViewModel(volId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /*val binding = FragmentVolDetailBinding.inflate(inflater)
        binding.volVM = volVM
        Log.d("onCreate", volVM.vol.toString())
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root*/

        val view = inflater.inflate(R.layout.fragment_vol_detail, container, false)

        volVM.dateLiveData.observe(viewLifecycleOwner) {
            view.findViewById<TextView>(R.id.date_value_tv).text = it.format(Formatters.DATE_FORMATTER)
        }
        volVM.heureLiveData.observe(viewLifecycleOwner) {
            view.findViewById<TextView>(R.id.heure_value_tv).text = it.format(Formatters.HEURE_FORMATTER)
        }
        volVM.dureeLiveData.observe(viewLifecycleOwner) {
            view.findViewById<TextView>(R.id.duree_value_tv).text = Utils.dureeToString(it)
        }
        volVM.siteLiveData.observe(viewLifecycleOwner){
            view.findViewById<TextView>(R.id.site_value_tv).text = it?.nom
        }

        view.findViewById<Button>(R.id.duree_change_btn).setOnClickListener {
            TimePickerFragment(object: TimePickerFragment.OnTimeSetListener{
                override fun onTimeSet(hourOfDay: Int, minute: Int) {
                    volVM.dureeLiveData.value = hourOfDay*60 + minute
                }
            }).show(childFragmentManager, "timePicker")
        }

        view.findViewById<Button>(R.id.heure_change_btn).setOnClickListener {
            TimePickerFragment(object: TimePickerFragment.OnTimeSetListener{
                override fun onTimeSet(hourOfDay: Int, minute: Int) {
                    volVM.heureLiveData.value = LocalTime.of(hourOfDay, minute)
                }
            }).show(childFragmentManager, "timePicker")
        }

        view.findViewById<Button>(R.id.date_change_btn).setOnClickListener {
            DatePickerFragment(object: DatePickerFragment.OnDateSetListener{
                override fun onDateSet(year: Int, month: Int, day: Int) {
                    volVM.dateLiveData.value = LocalDate.of(year, month, day)
                }

            }).show(childFragmentManager, "datePicker")
        }

        /*view.findViewById<FloatingActionButton>(R.id.delete_vol_btn).setOnClickListener {
            volVM.deleteVol()
            listener.onDeleteVol()
        }*/

        view.findViewById<Button>(R.id.site_change_btn).setOnClickListener {
            pickSiteRegistry.launch(SelectSiteActivity.getIntent(requireContext()))
        }

        return view
    }

    override fun onStop() {
        super.onStop()
        volVM.saveVol()
    }

    companion object {
        private const val EXTRA_VOL_ID = "com.application.carnetdevol.extra_vol_id"

        fun newInstance(volId: Int) =
            VolDetailFragment().apply {
                arguments = bundleOf(EXTRA_VOL_ID to volId)

            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDeleteVol) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnDeleteVol")
        }
    }

    interface OnDeleteVol {
        fun onDeleteVol()
    }

    private val pickSiteRegistry =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val numero = result.data?.getStringExtra(SelectSiteActivity.EXTRA_PICKED_SITE)
            if (numero != null) {
                Log.d("VolDetailFragment pickSite","$result")
                val site = volVM.getSite(numero)
                Log.d("VolDetailFragment pickSite","$site")
                volVM.siteLiveData.value = site
            }
        }
}