package com.application.carnetdevol.ui.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.application.carnetdevol.CarnetDeVolApplication
import com.application.carnetdevol.R
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.ui.adapter.SiteListAdapter
import com.application.carnetdevol.viewModel.SiteViewModel
import com.application.carnetdevol.viewModel.SiteViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SelectSiteFragment : Fragment(), SiteListAdapter.Callbacks {
    private lateinit var siteViewModel: SiteViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var researchField: EditText
    private lateinit var listener: OnInteractionListener

    private var adapter = SiteListAdapter(this)

    private val SCORE_SITE_THRESHOLD = 3
    private val SCORE_SITE_NOM_WEIGHT = 2
    private val SCORE_SITE_SOUS_NOM_WEIGHT = 1
    private val MAX_SIZE_FILTERED_LIST = 9

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_select_site, container, false)

        siteViewModel = SiteViewModelFactory(
            (requireActivity().application as CarnetDeVolApplication).repository)
            .create(SiteViewModel::class.java)

        siteViewModel.allSite.observe(viewLifecycleOwner) {
            lifecycleScope.launch(Dispatchers.Default) {
                filterList()
            }
        }

        recyclerView = view.findViewById(R.id.item_list)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext());

        researchField = view.findViewById(R.id.research_field)
        researchField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                val constraints = s?.trim()
                Log.d("SelectSiteFragment","afterTextChanged $constraints")
                lifecycleScope.launch(Dispatchers.Default) {
                    filterList(constraints.toString())
                }
            }
        })

        return view;
    }

    private suspend fun filterList(constraints: String? = null) {
        var filteredList = mutableListOf<Pair<Site, Int>>()
        if(constraints != null && constraints != ""){
            //Log.d("filterList site",siteViewModel.allSite.value.toString())
            siteViewModel.allSite.value?.let {
                for (site: Site in it){
                    var score: Int = 0
                    //Log.d("filterList site",site.toString())
                    for (wordConstraint in constraints.split(" ")) {
                        for (item in site.nom.split(" ")) {
                            //Log.d("filterList",item + "\t" + wordConstraint)
                            if (item.contains(wordConstraint, true)) score+=SCORE_SITE_NOM_WEIGHT
                        }

                        for (item in site.sous_nom.split(" ")) {
                            //Log.d("filterList",item + "\t" + wordConstraint)
                            if (item.contains(wordConstraint, true)) score+=SCORE_SITE_SOUS_NOM_WEIGHT
                        }

                        //if (site.codePostal.contains(wordConstraint, true)) score++
                    }

                    if (score >= SCORE_SITE_THRESHOLD) filteredList.add(Pair(site, score))
                    //Log.d("filterList",site.toString() + ": " + score)


                    if (filteredList.size > MAX_SIZE_FILTERED_LIST) break;
                }
            }
        }
        val finalList = filteredList.sortedBy { -it.second }
        val keys = mutableListOf<Site>()
        for (site in finalList){
            keys.add(site.first)
        }
        adapter.submitList(keys)
    }

    companion object {
        fun newInstance() =
            SelectSiteFragment().apply {

            }
    }

    override fun onSiteSelected(numero: String) {
        listener.onSiteSelected(numero)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }

    interface OnInteractionListener {
        fun onSiteSelected(numero: String)
    }
}