package com.application.carnetdevol.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.application.carnetdevol.R
import com.application.carnetdevol.model.Utils
import com.application.carnetdevol.model.Vol
import com.application.carnetdevol.ui.utils.Formatters
import com.application.carnetdevol.viewModel.VolViewModel

class VolAdapter(private val listener: Callbacks): ListAdapter<Vol, VolAdapter.VolViewHolder>(VolComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VolViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.vol_card, parent, false);
        return VolViewHolder(view, listener);
    }

    override fun onBindViewHolder(holder: VolViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class VolViewHolder(view: View, listener: Callbacks): RecyclerView.ViewHolder(view){
        val volDate = view.findViewById<TextView>(R.id.volDateTv)
        val volSite = view.findViewById<TextView>(R.id.volSiteTv)
        val volDuree = view.findViewById<TextView>(R.id.volDureeTv)

        var vol: Vol? = null
            private set

        init{
            view.setOnClickListener { vol?.let { listener.onVolSelected(it.id) } }
        }

        fun bind(vol: Vol){
            this.vol = vol
            volDate.text = vol.date.format(Formatters.DATE_FORMATTER)
            volSite.text = vol.site?.nom
            volDuree.text = Utils.dureeToString(vol.duree)
        }
    }

    class VolComparator(): DiffUtil.ItemCallback<Vol>(){
        override fun areItemsTheSame(oldItem: Vol, newItem: Vol): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Vol, newItem: Vol): Boolean {
            return oldItem.id == newItem.id
        }

    }

    interface Callbacks{
        fun onVolSelected(volId: Int)
    }
}