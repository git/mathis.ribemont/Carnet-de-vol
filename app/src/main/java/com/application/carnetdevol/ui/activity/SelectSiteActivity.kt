package com.application.carnetdevol.ui.activity

import android.content.Context
import android.content.Intent
import android.util.Log
import com.application.carnetdevol.R
import com.application.carnetdevol.ui.fragments.SelectSiteFragment

class SelectSiteActivity: SimpleFragmentActivity(), SelectSiteFragment.OnInteractionListener {
    private lateinit var masterFragment: SelectSiteFragment

    override fun createFragment() = SelectSiteFragment().also { masterFragment = it }
    override fun getLayoutResId() = R.layout.toolbar_activity

    companion object {
        private const val EXTRA_VOL_ID = "com.application.carnetdevol.extra_vol_id"

        fun getIntent(context: Context) =
            Intent(context, SelectSiteActivity::class.java).apply {

            }

        const val EXTRA_PICKED_SITE = "com.application.carnetdevol.ui.contract.extra_picked_site"
        const val PICK_SITE_CODE = 0
    }

    override fun onSiteSelected(numero: String) {
        Log.d("onSiteSelected","Site selectionné: ${numero}")
        setResult(PICK_SITE_CODE, Intent().putExtra(EXTRA_PICKED_SITE, numero))
        finish()
    }
}