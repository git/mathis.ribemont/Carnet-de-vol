package com.application.carnetdevol.ui.utils

import java.time.format.DateTimeFormatter

class Formatters {
    companion object {
        val HEURE_FORMATTER = DateTimeFormatter.ofPattern("HH:mm")
        val DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/YYYY")
    }
}