package com.application.carnetdevol.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.application.carnetdevol.R
import com.application.carnetdevol.api.BaliseMeteoManager
import com.application.carnetdevol.api.BaliseMeteoService
import com.application.carnetdevol.model.Balise
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

class BaliseInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_balise_info)

        BaliseMeteoManager().getBaliseInfo(83).enqueue(object : Callback<Balise>{
            override fun onFailure(call: Call<Balise>, t: Throwable) {
                Log.e("BaliseInfoActivity","Les informations n'ont pas pus être récupérées.\n"
                        + t.message
                        + "\n"
                        + t.stackTraceToString());
            }

            override fun onResponse(call: Call<Balise?>, response: Response<Balise>) {
                Log.d("BaliseInfoActivity","Données récupérées " + response.body()?.city);
            }
        });
    }
}