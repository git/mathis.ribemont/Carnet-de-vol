package com.application.carnetdevol.ui.activity

import android.content.Intent.getIntent
import android.os.Bundle
import android.util.Log
import com.application.carnetdevol.R
import com.application.carnetdevol.model.NEW_VOL_ID
import com.application.carnetdevol.ui.fragments.VolsListFragment

class VolsListActivity : SimpleFragmentActivity(), VolsListFragment.OnInteractionListener {
    private lateinit var masterFragment: VolsListFragment

    override fun getLayoutResId() = R.layout.toolbar_activity
    override fun createFragment() = VolsListFragment().also { masterFragment = it }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState != null)
            masterFragment = supportFragmentManager.findFragmentById(R.id.container_fragment) as VolsListFragment
    }

    override fun onVolSelected(volId: Int) {
        startActivity(VolDetailActivity.getIntent(this, volId))
    }

    override fun onAddNewVol() {
        addVol()
    }

    fun addVol() = startActivity(VolDetailActivity.getIntent(this, NEW_VOL_ID))
}