package com.application.carnetdevol.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.application.carnetdevol.R
import com.application.carnetdevol.model.NEW_VOL_ID
import com.application.carnetdevol.ui.fragments.VolDetailFragment

class VolDetailActivity : SimpleFragmentActivity(), VolDetailFragment.OnDeleteVol {
    override fun getLayoutResId() = R.layout.toolbar_activity
    override fun createFragment() = VolDetailFragment.newInstance(volId)

    private var volId = NEW_VOL_ID

    companion object {
        private const val EXTRA_VOL_ID = "com.application.carnetdevol.extra_vol_id"

        fun getIntent(context: Context, volId: Int) =
            Intent(context, VolDetailActivity::class.java).apply {
                putExtra(EXTRA_VOL_ID, volId)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        volId = intent.getIntExtra(EXTRA_VOL_ID, NEW_VOL_ID)
        super.onCreate(savedInstanceState)
    }

    override fun onDeleteVol() {
        finish()
    }
}