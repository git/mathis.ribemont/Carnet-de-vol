package com.application.carnetdevol.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.application.carnetdevol.R
import com.application.carnetdevol.model.Site

class SiteListAdapter(private val listener: Callbacks) : ListAdapter<Site, SiteListAdapter.SiteViewHolder>(SiteComparator()) {
    class SiteViewHolder(view: View, listener: Callbacks): RecyclerView.ViewHolder(view) {
        private val siteNom = view.findViewById<TextView>(R.id.site_nom)
        private val siteSousNom = view.findViewById<TextView>(R.id.site_sous_nom)

        private lateinit var site: Site

        init {
            view.setOnClickListener { site.let { listener.onSiteSelected(site.numero) }}
        }

        fun bind(site: Site){
            this.site = site
            siteNom.text = site.nom
            if(site.sous_nom != "")
                siteSousNom.text = site.sous_nom
            else siteSousNom.visibility = GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SiteViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.site_card, parent, false);
        return SiteViewHolder(view, listener);
    }

    override fun onBindViewHolder(holder: SiteViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class SiteComparator: DiffUtil.ItemCallback<Site>() {
        override fun areItemsTheSame(oldItem: Site, newItem: Site): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Site, newItem: Site): Boolean {
            return oldItem.numero.equals(newItem.numero)
        }

    }

    interface Callbacks {
        fun onSiteSelected(numero: String)
    }
}
