package com.application.carnetdevol.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.carnetdevol.CarnetDeVolApplication
import com.application.carnetdevol.R
import com.application.carnetdevol.model.Vol
import com.application.carnetdevol.ui.activity.SelectSiteActivity
import com.application.carnetdevol.ui.adapter.VolAdapter
import com.application.carnetdevol.viewModel.VolListViewModel
import com.application.carnetdevol.viewModel.VolListViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton

class VolsListFragment : Fragment(), VolAdapter.Callbacks {
    private lateinit var volsList: LiveData<List<Vol>>
    private lateinit var volViewModel: VolListViewModel
    private lateinit var volAdapter: VolAdapter
    private var listener: OnInteractionListener? = null

    interface OnInteractionListener {
        fun onVolSelected(volId: Int)
        fun onAddNewVol()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volAdapter = VolAdapter(this)

        volViewModel = VolListViewModelFactory(
            (requireActivity().application as CarnetDeVolApplication).repository)
            .create(VolListViewModel::class.java)

        volsList = volViewModel.allVols
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_vols_list, container, false);

        val volsListRv = view.findViewById<RecyclerView>(R.id.vols_list_rv)
        volsListRv.adapter = volAdapter
        volsListRv.layoutManager = LinearLayoutManager(requireContext());

        volsList.observe(viewLifecycleOwner) {
            volAdapter.submitList(volsList.value)
        }

        view.findViewById<FloatingActionButton>(R.id.add_new_vol_fab).setOnClickListener { listener?.onAddNewVol() }

        return view;
    }

    override fun onVolSelected(volId: Int) {
        listener?.onVolSelected(volId)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }
}