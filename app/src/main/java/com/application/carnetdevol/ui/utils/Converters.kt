package com.application.carnetdevol.ui.utils

import androidx.databinding.InverseMethod

object Converters {
    @JvmStatic
    fun stringToFloat(value: String) = if (value.isBlank()) 0f else value.toFloat()

    @JvmStatic
    @InverseMethod("stringToFloat")
    fun floatToString(value: Float) = if (value == 0f) "" else value.toString()
}