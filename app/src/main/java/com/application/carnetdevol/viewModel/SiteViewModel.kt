package com.application.carnetdevol.viewModel

import androidx.lifecycle.*
import com.application.carnetdevol.data.CarnetDeVolRepository
import com.application.carnetdevol.model.Site
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SiteViewModel(private val repository: CarnetDeVolRepository): ViewModel() {
    val allSite = MutableLiveData<List<Site>>()

    init {
        repository.getAllSites().observeForever {
            allSite.postValue(repository.getAllSites().value)
        }
    }
}

class SiteViewModelFactory(private val repository: CarnetDeVolRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SiteViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return SiteViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}