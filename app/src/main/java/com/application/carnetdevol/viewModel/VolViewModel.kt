package com.application.carnetdevol.viewModel

import android.util.Log
import androidx.lifecycle.*
import com.application.carnetdevol.api.BaliseMeteoManager
import com.application.carnetdevol.api.FfvlManager
import com.application.carnetdevol.data.CarnetDeVolDatabase
import com.application.carnetdevol.data.CarnetDeVolRepository
import com.application.carnetdevol.model.NEW_VOL_ID
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.model.Utils
import com.application.carnetdevol.model.Vol
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class VolViewModel(private val volId: Int): ViewModel() {

    private val repository = CarnetDeVolRepository(CarnetDeVolDatabase.getDatabase().volDao(),
        BaliseMeteoManager(), FfvlManager()
    )
    val vol = if (volId == NEW_VOL_ID) MutableLiveData(Vol()) else repository.getVol(volId)
    var status = VolStatus.Unchanged

    enum class VolStatus {
        Unchanged,
        ToSave,
        ToDelete
    }

    val dureeLiveData = MediatorLiveData<Int>()
    init {
        dureeLiveData.addSource(vol) { dureeLiveData.postValue(it.duree) }
        dureeLiveData.observeForever { newDuree ->
            vol.value?.let {
                if (it.duree != newDuree) {
                    it.duree = newDuree
                    status = VolStatus.ToSave
                }
            }
        }
    }

    val dateLiveData = MediatorLiveData<LocalDate>()
    init {
        dateLiveData.addSource(vol) { dateLiveData.postValue(it.date) }
        dateLiveData.observeForever { newDate ->
            vol.value?.let {
                if (it.date != newDate) {
                    it.date = newDate
                    status = VolStatus.ToSave
                }
            }
        }
    }

    val heureLiveData = MediatorLiveData<LocalTime>()
    init {
        heureLiveData.addSource(vol) { heureLiveData.postValue(it.heure) }
        heureLiveData.observeForever { newHeure ->
            vol.value?.let {
                if (it.heure != newHeure) {
                    it.heure = newHeure
                    status = VolStatus.ToSave
                }
            }
        }
    }

    val siteLiveData = MediatorLiveData<Site?>()
    init {
        siteLiveData.addSource(vol) { siteLiveData.postValue(it.site) }
        siteLiveData.observeForever { newSite ->
            vol.value?.let {
                if (it.site != newSite) {
                    it.site = newSite
                    Log.d("VolViewModel siteLiveData","Nouveau site: ${it.site} ${newSite}")
                    status = VolStatus.ToSave
                }
            }
        }
    }

    fun saveVol() = vol.value?.let {
        //Log.d("saveVol", "Duree: ${vol.value?.duree?.let { it1 -> Utils.dureeToString(it1) }}")
        viewModelScope.launch {
            if (status == VolStatus.ToSave){
                if (it.id == NEW_VOL_ID){
                    repository.insert(it)
                }
                else repository.update(it)
            } /*else if (status == VolStatus.ToDelete){
                repository.delete(it)
            }*/
        }
    }

    fun deleteVol() = vol.value?.let {
        viewModelScope.launch {
            status = VolStatus.ToDelete
            repository.delete(it)
        }
    }

    fun getSite(numero: String): Site?{
        return repository.getSite(numero)
    }
}