package com.application.carnetdevol.viewModel

import android.util.Log
import androidx.lifecycle.*
import com.application.carnetdevol.data.CarnetDeVolRepository
import com.application.carnetdevol.model.Site
import com.application.carnetdevol.model.Vol
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class VolListViewModel(private val repository: CarnetDeVolRepository): ViewModel() {
    val allVols = /*MutableLiveData<List<Vol>>()*/ repository.getAllVols()

    init {

    }

    fun insert(vol: Vol) = repository.insert(vol)
}

class VolListViewModelFactory(private val repository: CarnetDeVolRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(VolListViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return VolListViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}