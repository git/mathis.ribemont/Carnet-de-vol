package com.application.carnetdevol.api

import com.application.carnetdevol.model.Site
import retrofit2.Call
import retrofit2.http.GET

interface FfvlService {
    @GET("/json/sites.json")
    fun getAllSites(): Call<List<Site>>
}