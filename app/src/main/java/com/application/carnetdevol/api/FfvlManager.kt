package com.application.carnetdevol.api

import com.application.carnetdevol.model.Site
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FfvlManager {
    private val URL = "http://data.ffvl.fr"

    fun getAllSites(): Call<List<Site>> {
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(FfvlService::class.java)

        return service.getAllSites()
    }
}