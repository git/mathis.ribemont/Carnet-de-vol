package com.application.carnetdevol.api

import android.util.Log
import com.application.carnetdevol.model.Balise
import com.application.carnetdevol.model.Site
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BaliseMeteoManager {
    private val URL = "https://balisemeteo.com";

    fun getBaliseInfo(idBalise: Int): Call<Balise>{
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        val service = retrofit.create(BaliseMeteoService::class.java);

        val requete = service.getBaliseInfo(idBalise);
        return requete;
    }


}