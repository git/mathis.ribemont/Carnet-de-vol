package com.application.carnetdevol.api

import com.application.carnetdevol.model.Balise
import com.application.carnetdevol.model.Site
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BaliseMeteoService {
    @GET("balise_json.php")
    fun getBaliseInfo(@Query("idBalise") idBalise: Int): Call<Balise>;
}