package com.application.carnetdevol.model

import kotlin.math.floor

class Utils {

    companion object {
        fun dureeToString(duree: Int): String{
            val heure = duree/60
            val minute = duree%60
            val s = StringBuilder("${heure}h")
            if(minute < 10) s.append("0")
            s.append(minute)
            return s.toString()
        }
    }
}