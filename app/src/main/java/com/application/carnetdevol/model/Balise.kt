package com.application.carnetdevol.model

data class Balise(val idBalise:Int,
                  val city:String,
                  val latitude:String,
                  val longitude:String,
                  val altitude:String,
                  val vitesseVentMoyen:Int,
                  val vitesseMax:Int,
                  val vitesseVentMin:Int,
                  val temperature:Int,
                  val unitVent:String,
                  val lum:Int,
                  val photoURI:String) {
}