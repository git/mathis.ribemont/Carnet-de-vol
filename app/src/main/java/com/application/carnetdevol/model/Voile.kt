package com.application.carnetdevol.model

import androidx.room.Entity

@Entity(tableName = "voile_table")
class Voile(var id: Int,
            var marque:String,
            var modele:String) {
}