package com.application.carnetdevol.model

class Manager {
    private final var vols = mutableListOf<Vol>();
    private final var sites = mutableListOf<Site>();

    fun getVols(): List<Vol>{
        return vols.toList();
    }

    fun addVol(vol:Vol){
        vols.add(vol);
    }

    fun removeVol(vol:Vol){
        vols.remove(vol);
    }

    fun addVol(site:Site){
        sites.add(site);
    }

    fun removeSite(site:Site){
        sites.remove(site);
    }
}