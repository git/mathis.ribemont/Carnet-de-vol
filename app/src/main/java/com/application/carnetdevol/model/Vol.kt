package com.application.carnetdevol.model

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import kotlin.math.floor

const val NEW_VOL_ID = 0

class Vol(var id: Int = NEW_VOL_ID,
          var date: LocalDate = LocalDate.now(),
          var heure: LocalTime = LocalTime.now(),
          var site: Site? = null,
          var duree: Int = 0,
          /*var voile: Voile*/) {
}