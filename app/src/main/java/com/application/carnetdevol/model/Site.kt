package com.application.carnetdevol.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.application.carnetdevol.data.SITE_TABLE_NAME
import com.google.gson.annotations.SerializedName

@Entity(tableName = SITE_TABLE_NAME)
class Site(@PrimaryKey val sid: String,
           val numero: String,
           val nom: String,
           val sous_nom: String,
           val cp: String,
           val ville: String,
           val vol: String,
           val sousType: String?,
           val pratiques: String,
           val lat:String,
           val lon:String,
           val alt:String,
           val access:String,
           val orientation:String,
           val vent_favo:String,
           val vent_defavo:String,
           val restrictions:String,
           val dangers:String,
           val balise: String) {

    override fun toString(): String {
        return "$numero $cp $nom $sous_nom"
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other) || numero == (other as Site).numero
    }
}